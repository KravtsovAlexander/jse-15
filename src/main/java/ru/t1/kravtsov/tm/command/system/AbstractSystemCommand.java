package ru.t1.kravtsov.tm.command.system;

import ru.t1.kravtsov.tm.api.service.ICommandService;
import ru.t1.kravtsov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
