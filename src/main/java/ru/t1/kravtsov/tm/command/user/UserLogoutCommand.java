package ru.t1.kravtsov.tm.command.user;

public class UserLogoutCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Logout.";

    public static final String NAME = "logout";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
