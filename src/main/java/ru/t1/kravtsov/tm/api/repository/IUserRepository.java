package ru.t1.kravtsov.tm.api.repository;

import ru.t1.kravtsov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    void deleteAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    Boolean doesLoginExist(String login);

    Boolean doesEmailExist(String email);

}
