package ru.t1.kravtsov.tm.api.repository;

import ru.t1.kravtsov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task add(Task task);

    void deleteAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task project);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    List<Task> removeByName(String name);

    List<Task> findAllByProjectId(String projectId);

}
